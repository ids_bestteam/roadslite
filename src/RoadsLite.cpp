//============================================================================
// Name        : RoadsLite.cpp
// Author      : Michael Liu
// Version     : 0
// Copyright   : None... for now
// Description : Sorry Brennan, but this exists now.
//============================================================================

#include <iostream>

#include "UI/NCursesApplication.h"

using namespace std;

int main() {
	NCursesApplication* app = new NCursesApplication();
	try {
		// Run the game
		app -> run();
	} catch (std::exception& e) {
		std::cout << "Caught Exception!" << std::endl;
		std::cout << e.what() << std::endl;

		delete app;
		return 1;
	}

	delete app;
	return 0;
}
