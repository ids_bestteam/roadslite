/*
 * Application.h
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#ifndef UI_NCURSESAPPLICATION_H_
#define UI_NCURSESAPPLICATION_H_

#include <vector>
#include <set>

#include "../util/Point.h"

#include "../model/GameManager.h"

typedef std::vector< std::vector <char> > Matrix;

class NCursesApplication {
public:
	NCursesApplication();
	virtual ~NCursesApplication();

	void run();

	Point getTopLeftPoint();
	Point getBottomRightPoint();
	int getWindowHeight();
	int getWindowWidth();

	void addDirtyPoint(Point p);

	void debug(std::string msg);
	void debug1(std::string msg);

	// This function should be used sparingly
	//	Only when it is absolutely necessary to redraw the background
	void drawBackground();

private:
	int windowHeight, windowWidth;

	bool isRunning;
	GameManager gameManager;

	std::set<Point> dirtyPoints;
	Matrix background; // Please store points in y, x format

	void handleInput();
	void render();

	void redrawDirtyPoints();

	void handleWindowResizedEvent();

	void loadBackground();

};

#endif /* UI_NCURSESAPPLICATION_H_ */
