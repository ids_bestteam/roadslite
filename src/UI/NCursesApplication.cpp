/*
 * Application.cpp
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */


#include "NCursesApplication.h"

#include <ncurses.h>

#define FRAMERATE 3

/**
 * Initializes NCurses and GameManager
 */
NCursesApplication::NCursesApplication()
	: isRunning(true),
	  gameManager(this) {

	initscr();
	noecho();
	cbreak();
	timeout(1000 / FRAMERATE);
	curs_set(0);
	keypad(stdscr, true);

	getmaxyx(stdscr, windowHeight, windowWidth);
	gameManager.centreViewOnPlayer();
	drawBackground();
}

/**
 * Shuts down NCurses and ends the game. Restores terminal properties
 */
NCursesApplication::~NCursesApplication() {
	endwin();
}

/**
 * Main NCurses loop.
 * Runs event handlers and draws sprites
 */
void NCursesApplication::run() {
	isRunning = true;
	render();
	refresh();
	while (isRunning) {
		handleInput();
		render();
		refresh();
	}
}

/**
 * Get world coordinates of top left corner of screen
 *
 * @return top left corner of screen in world coordinates
 */
Point NCursesApplication::getTopLeftPoint() {
	// TODO: Implement this function
	return Point {0, 0};
}

/**
 * Get world coordinates of bottom right corner of screen
 *
 * @return bottom right corner of screen in world coordinates
 */
Point NCursesApplication::getBottomRightPoint() {
	// TODO: Implement this function
	return Point {0, 0};
}

/**
 * Get the height of the window
 *
 * @return height of the window
 */
int NCursesApplication::getWindowHeight() {
	return getmaxy(stdscr);
}

/**
 * Get the width of the window
 *
 * @return width of the window
 */
int NCursesApplication::getWindowWidth() {
	return getmaxx(stdscr);
}

void NCursesApplication::addDirtyPoint(Point p) {
	dirtyPoints.insert(p);
}

void NCursesApplication::debug(std::string msg) {
	mvaddstr(0, 0, msg.c_str());
}

void NCursesApplication::debug1(std::string msg) {
	mvaddstr(1, 0, msg.c_str());
}

/**
 * Handles keyboard events. Dispatches movement events to GameManager
 */
void NCursesApplication::handleInput() {
	int keyCode = getch();

	gameManager.handleKeyEvent(keyCode);
	switch (keyCode) {
	case KEY_RESIZE :
		handleWindowResizedEvent();
		break;
	case 'q' : // Pass through
	case 'Q' :
		isRunning = false;
		break;
	default:
		break;
	}
}

/**
 * Draws all sprites to stdscr
 */
void NCursesApplication::render() {
	redrawDirtyPoints();
}

/**
 * Redraws background on screen locations marked dirty
 */
void NCursesApplication::redrawDirtyPoints() {
	for (Point p : dirtyPoints) {
		mvaddch(p.y, p.x, gameManager.getCharAtScreenCoord(p));
	}
	dirtyPoints.clear();
}

/**
 * Handles when the window is resized.
 * Loads larger background, and redraws background and sprites
 */
void NCursesApplication::handleWindowResizedEvent() {
	getmaxyx(stdscr, windowHeight, windowWidth);
	gameManager.centreViewOnPlayer();
	drawBackground();
	render();
}

/**
 * Draws all points of background to screen.
 */
void NCursesApplication::drawBackground() {
	for (int y = 0; y < windowHeight; ++y) {
		for (int x = 0; x < windowWidth; ++x) {
			mvaddch(y, x, gameManager.getCharAtScreenCoord(Point {x, y}));
		}
	}
}
