/*
 * Observer.h
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#ifndef ABSTRACTIONS_OBSERVER_OBSERVER_H_
#define ABSTRACTIONS_OBSERVER_OBSERVER_H_

class Observer {
public:
	virtual void update(void* data) = 0;
protected:
	~Observer() {};
};

#endif /* ABSTRACTIONS_OBSERVER_OBSERVER_H_ */
