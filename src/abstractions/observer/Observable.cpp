/*
 * Observable.cpp
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#include "Observable.h"
#include "Observer.h"

void Observable::addObserver(Observer* observer) {
	observers.insert(observer);
}

void Observable::removeObserver(Observer* observer) {
	observers.erase(observer);
}

void Observable::notifyObservers(void* data) {
	for (Observer* observer : observers) {
		observer -> update(data);
	}
}
