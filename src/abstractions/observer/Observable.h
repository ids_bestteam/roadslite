/*
 * Observable.h
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#ifndef ABSTRACTIONS_OBSERVER_OBSERVABLE_H_
#define ABSTRACTIONS_OBSERVER_OBSERVABLE_H_

#include <set>

#include "Observer.h"

class Observable {
public:
	void addObserver(Observer* observer);
	void removeObserver(Observer* observer);

	void notifyObservers(void* data);

private:
	std::set<Observer*> observers;
};

#endif /* ABSTRACTIONS_OBSERVER_OBSERVABLE_H_ */
