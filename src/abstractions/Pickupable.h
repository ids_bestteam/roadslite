/*
 * Pickupable.h
 *
 *  Created on: Apr 12, 2018
 *      Author: michael
 */

#ifndef ABSTRACTIONS_PICKUPABLE_H_
#define ABSTRACTIONS_PICKUPABLE_H_

class Pickupable {
public:
	virtual void onPickup() = 0;
protected:
	~Pickupable() {}
};

#endif /* ABSTRACTIONS_PICKUPABLE_H_ */
