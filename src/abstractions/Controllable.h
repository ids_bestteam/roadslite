/*
 * Moveable.h
 *
 *  Created on: Apr 11, 2018
 *      Author: michael
 */

#ifndef ABSTRACTIONS_CONTROLLABLE_H_
#define ABSTRACTIONS_CONTROLLABLE_H_

class Controllable {
public:
	virtual void handleKeyEvent(int keyCode) = 0;
protected:
	~Controllable() {};
};

#endif /* ABSTRACTIONS_CONTROLLABLE_H_ */
