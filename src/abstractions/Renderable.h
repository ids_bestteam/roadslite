/*
 * Renderable.h
 *
 *  Created on: Apr 12, 2018
 *      Author: michael
 */

#ifndef ABSTRACTIONS_RENDERABLE_H_
#define ABSTRACTIONS_RENDERABLE_H_

class Renderable {
	virtual char getSprite() = 0;
protected:
	~Renderable() {}
};

#endif /* ABSTRACTIONS_RENDERABLE_H_ */
