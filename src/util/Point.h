/*
 * Point.h
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#ifndef MODEL_POINT_H_
#define MODEL_POINT_H_

#include <iostream>

struct Point {
	int x, y;

};

inline bool operator==(const Point& lhs, const Point& rhs) {
	return (lhs.x == rhs.x) && (lhs.y == rhs.y);
}

inline bool operator!=(const Point& lhs, const Point& rhs) {
	return !(lhs == rhs);
}

inline bool operator< (const Point& lhs, const Point& rhs) {
	return (lhs.x < rhs.x) || ((lhs.x == rhs.x) && (lhs.y < rhs.y));
}

inline bool operator> (const Point& lhs, const Point& rhs) {
	return (rhs < lhs);
}

inline Point operator+ (const Point& lhs, const Point& rhs) {
	return Point{lhs.x + rhs.x, lhs.y + rhs.y};
}

inline Point operator- (const Point& lhs, const Point& rhs) {
	return Point{lhs.x - rhs.x, lhs.y - rhs.y};
}

inline std::ostream& operator<<(std::ostream& os, const Point pt) {
	os << pt.x << " " << pt.y;
	return os;
}

namespace std {
template<> struct hash<Point> {
	std::size_t operator()(const Point& p) const {
		return (std::hash<int>()(p.x) ^ ((std::hash<int>()(p.y))<<1));
	}
};
}

#endif /* MODEL_POINT_H_ */
