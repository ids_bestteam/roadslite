/*
 * Util.h
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#ifndef MODEL_UTIL_H_
#define MODEL_UTIL_H_

#include "Point.h"

class Util {
public:
	static bool isPointInRect(Point topLeft, Point bottomRight, Point point);
};

#endif /* MODEL_UTIL_H_ */
