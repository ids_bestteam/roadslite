/*
 * Util.cpp
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#include "Util.h"

/**
 * Computes whether the point is inside the provided rectangle
 *
 * @param topLeft		Top left corner of rect
 * @param bottomRight	Bottom right corner of rect
 * @param point			Point to test
 * @return	whether point is in rectangle defined by topLeft and bottomRight
 */
bool Util::isPointInRect(Point topLeft, Point bottomRight, Point point) {
	// TODO: Implement this function
	bool xCoordWithinBoundary = (topLeft.x <= point.x) &&
								(point.x <= bottomRight.x);
	bool yCoordWithinBoundary = (topLeft.y <= point.y) &&
								(point.y <= bottomRight.y);
	return 	xCoordWithinBoundary && yCoordWithinBoundary;
}
