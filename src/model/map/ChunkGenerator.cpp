/*
 * ChunkGenerator.cpp
 *
 *  Created on: May 3, 2018
 *      Author: michael
 */

#include <cstdlib>

#include "ChunkGenerator.h"

ChunkGenerator::ChunkGenerator() {

}

ChunkGenerator::~ChunkGenerator() {
	// TODO Auto-generated destructor stub
}

Chunk* ChunkGenerator::generateChunk(Point region) {
	Chunk* chunk = new Chunk(region);
	for (int x = 0; x < Chunk::CHUNK_SIZE; x++) {
		for (int y = 0; y < Chunk::CHUNK_SIZE; y++) {
			chunk -> setChar(Point{x, y}, rand()%10 == 1 ? '#' : ' ');
		}
	}
	return chunk;
}
