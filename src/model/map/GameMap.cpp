/*
 * Map.cpp
 *
 *  Created on: May 3, 2018
 *      Author: michael
 */

#include <cmath>
#include <algorithm>
#include <fstream>

#include "GameMap.h"
#include "../../util/Util.h"

GameMap::~GameMap() {
	// TODO Auto-generated destructor stub
	for (auto element : chunkMap) {
		delete element.second;
	}
}

GameMap::GameMap()
	: renderDistance(2),
	  mapCentre(Point{0, 0}) {
	// TODO Auto-generated constructor stub

}

char GameMap::getChar(Point p) {
	Point region = getRegion(p);
	auto it = chunkMap.find(region);
	if (it != chunkMap.end()) {
		return it -> second -> getChar(Point{p.x - (region.x * Chunk::CHUNK_SIZE),
											 p.y - (region.y * Chunk::CHUNK_SIZE)});
	} else {
		return (char) region.y + '9';
	}
}

Point GameMap::getRegion(const Point& point) {
	int x = std::floor(((float) point.x) / Chunk::CHUNK_SIZE);
	int y = std::floor(((float) point.y) / Chunk::CHUNK_SIZE);
	return Point{x, y};
}


void GameMap::recalculateChunks(Point centre) {
	Point region = getRegion(centre);
	unloadOldChunks(region);
	loadNewChunks(region);
	mapCentre = region;
}

void GameMap::loadNewChunks(Point newCentre) {
	for (int xx = newCentre.x - renderDistance;
			xx <= newCentre.x + renderDistance;
			xx++) {
		for (int yy = newCentre.y - renderDistance;
				yy <= newCentre.y + renderDistance;
				yy++) {
			if (chunkMap.count(Point{xx, yy}) == 0) {
				loadChunk(Point{xx, yy});
			}
		}
	}
}

void GameMap::unloadOldChunks(Point newCentre) {
	for (auto itr = chunkMap.begin(); itr != chunkMap.end();) {
		if (!isChunkInRangeOfPoint(newCentre, itr -> first) &&
				!isAlwaysLoaded(itr -> first)) {
			chunkMap.erase(itr -> first);
		}
		itr++;
	}
}

bool GameMap::isChunkInRangeOfPoint(Point pivot, Point region) {
	return (Util::isPointInRect(Point {pivot.x - renderDistance,
									   pivot.y - renderDistance },
								Point {pivot.x + renderDistance,
									   pivot.y + renderDistance },
								region));
}

// TODO: complete this
void GameMap::loadChunk(Point region) {
	std::ifstream chunkData(getFilename(region));
	if (chunkData.good()) {
		chunkMap[region] = parseChunkFile(chunkData);
	} else {
		chunkMap[region] = ChunkGenerator::generateChunk(region);
	}
}

Chunk* GameMap::parseChunkFile(const std::ifstream& chunkFile) {
	return NULL;
}

std::string GameMap::getFilename(Point p) {
	return  std::to_string(p.x) +
			"_" +
			std::to_string(p.y) +
			".region";
}

void GameMap::addAlwaysLoadedChunk(Point region) {
	alwaysLoaded.insert(region);
}

void GameMap::removeAlwaysLoadedChunk(Point region) {
	alwaysLoaded.erase(region);
}

bool GameMap::isAlwaysLoaded(Point region) {
	return (alwaysLoaded.count(region)) == 1;
}
