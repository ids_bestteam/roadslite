/*
 * Map.h
 *
 *  Created on: May 3, 2018
 *      Author: michael
 */

#ifndef MODEL_MAP_GAMEMAP_H_
#define MODEL_MAP_GAMEMAP_H_

#include <map>
#include <unordered_set>

#include "../../util/Point.h"
#include "Chunk.h"
#include "ChunkGenerator.h"

/**
 * Contains all chunks currently loaded by game
 * Loads chunks from file, and generates new chunks when none available
 */
class GameMap {
public:
	virtual ~GameMap();
	GameMap();
	
	char getChar(Point coord);
	Point getRegion(const Point& point);
	void recalculateChunks(Point centre);

private:
	int renderDistance;
	Point mapCentre;
	std::map<Point, Chunk*> chunkMap;
	std::unordered_set<Point> alwaysLoaded;
	ChunkGenerator generator;

	void loadNewChunks(Point newCentre);
	void unloadOldChunks(Point newCentre);
	bool isChunkInRangeOfPoint(Point pivot, Point region);
	void loadChunk(Point region);
	Chunk* parseChunkFile(const std::ifstream& chunkFile);
	std::string getFilename(Point p);
	void unloadChunk(Point region);
	void loadChunkFromFile(Point region);
	void generateNewChunk(Point region);

	inline void addAlwaysLoadedChunk(Point region);
	inline void removeAlwaysLoadedChunk(Point region);
	inline bool isAlwaysLoaded(Point region);

};

#endif /* MODEL_MAP_GAMEMAP_H_ */
