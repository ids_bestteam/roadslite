/*
 * Chunk.cpp
 *
 *  Created on: May 2, 2018
 *      Author: michael
 */

#include "Chunk.h"

Chunk::Chunk(Point region)
	: region(region) {

}

Chunk::Chunk(int region_x, int region_y)
	: Chunk(Point{region_x, region_y}) {

}

Chunk::Chunk(Point region, std::string filename)
	: region(region) {
	// TODO: load chunk from file
}

Chunk::Chunk(int region_x, int region_y, std::string filename)
	: Chunk(Point {region_x, region_y}, filename) {
	// TODO: load chunk from file
}

/**
 *
 */
Chunk::~Chunk() {
	// TODO Auto-generated destructor stub
}

char Chunk::getChar(Point p) {
	if (p.x < CHUNK_SIZE && p.y < CHUNK_SIZE) {
		return map[p.x][p.y];
	} else {
		throw "Point not in range";
	}
}

void Chunk::setChar(Point p, char c) {
	if (p.x < CHUNK_SIZE && p.y < CHUNK_SIZE) {
		map[p.x][p.y] = c;
	}
}
