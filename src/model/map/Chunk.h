/*
 * Chunk.h
 *
 *  Created on: May 2, 2018
 *      Author: michael
 */

#ifndef MODEL_CHUNK_H_
#define MODEL_CHUNK_H_

#include <string>
#include <vector>

#include "../../util/Point.h"

class Chunk {
public:
	static const int CHUNK_SIZE = 8;

	Chunk(Point region);
	Chunk(int region_x, int region_y);
	Chunk(Point region, std::string filename);
	Chunk(int region_x, int region_y, std::string filename);
	virtual ~Chunk();

	char getChar(Point p);
	void setChar(Point p, char c);

private:
	// Chunk Coordinates
	Point region;

	char map[CHUNK_SIZE][CHUNK_SIZE];
};

#endif /* MODEL_CHUNK_H_ */
