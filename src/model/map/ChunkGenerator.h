/*
 * ChunkGenerator.h
 *
 *  Created on: May 3, 2018
 *      Author: michael
 */

#ifndef MODEL_MAP_CHUNKGENERATOR_H_
#define MODEL_MAP_CHUNKGENERATOR_H_

#include "Chunk.h"

class ChunkGenerator {
public:
	ChunkGenerator();
	virtual ~ChunkGenerator();

	static Chunk* generateChunk(Point region);
};

#endif /* MODEL_MAP_CHUNKGENERATOR_H_ */
