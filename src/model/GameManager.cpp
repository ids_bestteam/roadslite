/*
 * GameManager.cpp
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#include <iostream>
#include <ncurses.h>	// for key code constant definitions

#include "GameManager.h"
#include "../UI/NCursesApplication.h"

/**
 * Makes a new GameManager
 *
 * @param app NCursesApplication hosting the game
 * @param initialTopLeftPoint	top left corner of screen
 * @param initialScreenWidth	width of screen
 * @param initialScreenHeight	height of screen
 */
GameManager::GameManager( NCursesApplication* app )
	: app(app) {

	player = Player::getInstance(app, this, worldSpawn);
	actor = player;
	objects[player -> getPosition()] = player;
	gameMap.recalculateChunks(player -> getPosition());
	app -> drawBackground();
}

GameManager::~GameManager() {
}

/**
 * Called when a GameObject moves. GameManager should change position of
 * GameObject in the position matrix
 *
 * @param data Pointer to Point object representing old position of object
 */
void GameManager::update(void* data) {
	Point p = *((Point*) data);
	GameObject* go = objects[p];
	objects.erase(p);
	objects[go -> getPosition()] = go;
	app -> addDirtyPoint(getScreenCoord(p));
	app -> addDirtyPoint(getScreenCoord(go -> getPosition()));
}

void GameManager::handleKeyEvent(int keyCode) {
	// Handle events locally
	switch (keyCode) {

	}

	// Delegate key event handling to other objects

	// Update visible area if necessary
	static Point currRegion;
	static Point prevRegion;

	prevRegion = gameMap.getRegion(player -> getPosition());
	actor -> handleKeyEvent(keyCode);
	currRegion = gameMap.getRegion(player -> getPosition());

	if (prevRegion != currRegion) {
		gameMap.recalculateChunks(player -> getPosition());
		app -> drawBackground();
	}
}

Player* GameManager::getPlayer() {
	return player;
}

char GameManager::getCharAtScreenCoord(Point p) {
	auto it = objects.find(getGlobalCoord(p));
	if (it != objects.end()) {
		return it -> second -> getSprite();
	} else {
		return gameMap.getChar(getGlobalCoord(p));
	}
}

const std::map<Point, GameObject*>& GameManager::getObjects() const {
	return objects;
}

Point GameManager::getGlobalCoord(Point screenCoord) {
	return topLeft + screenCoord;
}

Point GameManager::getScreenCoord(Point globalCoord) {
	return globalCoord - topLeft;
}

void GameManager::centreViewOnPlayer() {
	topLeft = player -> getPosition() - Point{app -> getWindowWidth() / 2, app -> getWindowHeight() / 2};
}
