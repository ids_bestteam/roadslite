/*
 * GameObject.h
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#ifndef MODEL_GAMEOBJECT_H_
#define MODEL_GAMEOBJECT_H_

#include "../util/Point.h"

#include "../abstractions/Renderable.h"
#include "../abstractions/observer/Observable.h"


class NCursesApplication;
class GameManager;

/**
 * Represents an object in the game world.
 * eg. 	The player
 * 		Enemies
 * 		Pickups
 * 		Items
 * 		Entities
 */

class GameObject : public Renderable, public Observable {
public:
	GameObject(NCursesApplication* app, GameManager* gm, Point position);
	virtual ~GameObject();

	virtual char getSprite() = 0;

	virtual Point getPosition();

protected:
	// Reference to parent app
	NCursesApplication* app;
	// Previous global position of GameObject
	Point prevPosition;
	// Global position of GameObject in world coordinates
	Point position;
};

#endif /* MODEL_GAMEOBJECT_H_ */
