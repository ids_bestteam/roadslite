/*
 * Player.h
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#ifndef MODEL_PLAYER_H_
#define MODEL_PLAYER_H_

#include "../abstractions/Controllable.h"
#include "GameObject.h"

class Player: public GameObject, public Controllable {
public:

	static Player* getInstance();
	static Player* getInstance(NCursesApplication* app, GameManager* gm, Point position);

	virtual char getSprite() override;

	virtual void handleKeyEvent(int keyCode) override;

private:
	Player();
	Player(NCursesApplication* app, GameManager* gm, Point position);

	static Player* instance;
};

#endif /* MODEL_PLAYER_H_ */
