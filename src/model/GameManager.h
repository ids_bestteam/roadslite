/*
 * GameManager.h
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#ifndef MODEL_GAMEMANAGER_H_
#define MODEL_GAMEMANAGER_H_

#include <map>

#include "../abstractions/observer/Observer.h"

#include "../util/Point.h"

#include "Player.h"
#include "map/GameMap.h"

/**
 * A class for managing the state of the game at any given time
 * Stores a handle to the player
 */
class GameManager : public Observer {
public:
	GameManager(NCursesApplication* app);
	virtual ~GameManager();

	Point getTopLeftPoint();
	Point getBottomRightPoint();

	virtual void update(void* data) override;

	void handleKeyEvent(int keyCode);

	Player* getPlayer();

	char getCharAtScreenCoord(Point p);

	const std::map<Point, GameObject*>& getObjects() const;

	Point getGlobalCoord(Point p);
	Point getScreenCoord(Point p);

	void centreViewOnPlayer();

private:
	NCursesApplication* app;

	// Top left corner of rendered area
	Point topLeft;

	// Point where new players should spawn
	Point worldSpawn;

	// Composition relationship with player
	Player* player;

	// A pointer to the currently controlled entity
	Controllable* actor;

	// The entire background
	GameMap gameMap;

	// All of the gameObjects
	std::map<Point, GameObject*> objects;
};

#endif /* MODEL_GAMEMANAGER_H_ */
