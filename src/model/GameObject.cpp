/*
 * GameObject.cpp
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */

#include "GameObject.h"

#include "../util/Point.h"
#include "../util/Util.h"
#include "../UI/NCursesApplication.h"
#include "GameManager.h"

GameObject::GameObject(NCursesApplication* app, GameManager* gm, Point position) {
	this -> app = app;
	this -> position = position;
	prevPosition = position;

	addObserver(gm);
}

GameObject::~GameObject() {
	// TODO Auto-generated destructor stub
}

Point GameObject::getPosition() {
	return position;
}
