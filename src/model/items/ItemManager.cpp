/*
 * ItemManager.cpp
 *
 *  Created on: Apr 19, 2018
 *      Author: michael
 */

#include <regex>

#include "ItemManager.h"

/**
 * ItemManager destructor
 * Performs garbage collection on all items
 */
ItemManager::~ItemManager() {

	// No item should appear in the items map twice
	// So we can safely delete each of them with no checks
	for (std::pair<Point, Item*> pair : items) {
		Item* item = pair.second;
		delete item;
	}

}

/**
 * Loads items from ITEMS_FILE into item map.
 * Dispatches any additional loading of special properties to respective managers.
 */
void ItemManager::loadItems() {
	// TODO: Implement this method
	std::regex regex("");

}

/**
 * Destroys item as well as any corresponding special properties in managers.
 *
 * @param item	Item to be destroyed
 */
void ItemManager::destroyItem(Item* item) {
	// TODO: complete this implementation
	delete item;
}
