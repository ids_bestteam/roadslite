/*
 * CraftingItem.h
 *
 *  Created on: Apr 13, 2018
 *      Author: michael
 */

#ifndef MODEL_ITEMS_CRAFTINGITEM_H_
#define MODEL_ITEMS_CRAFTINGITEM_H_

#include "../Item.h"

class CraftingItem: public Item {
public:
	CraftingItem(NCursesApplication* app, GameManager* gm, Point position);
};

#endif /* MODEL_ITEMS_CRAFTINGITEM_H_ */
