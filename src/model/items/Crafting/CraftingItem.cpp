/*
 * CraftingItem.cpp
 *
 *  Created on: Apr 13, 2018
 *      Author: michael
 */

#include "CraftingItem.h"

CraftingItem::CraftingItem(NCursesApplication* app, GameManager* gm, Point position)
	: Item(app, gm, position) {
	typeTags &= TYPES::CRAFTING;
}

