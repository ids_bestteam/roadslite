/*
 * Item.h
 *
 *  Created on: Apr 12, 2018
 *      Author: michael
 */

#ifndef MODEL_ITEM_H_
#define MODEL_ITEM_H_

#include <bitset>

#include "../GameObject.h"
#include "../../abstractions/Pickupable.h"

class Item: public GameObject, public Pickupable {
public:
	Item(NCursesApplication* app, GameManager* gm, Point position);
	Item(NCursesApplication* app, GameManager* gm, Point position, char* name, char* description);
	virtual ~Item();

	virtual void onPickup() = 0;

protected:
	enum TYPES { NONE,
				 CONSUMABLE,
				 CRAFTING,
				 WEAPON };
	std::bitset<8> typeTags;

	const char* name;
	const char* description;
};

#endif /* MODEL_ITEM_H_ */
