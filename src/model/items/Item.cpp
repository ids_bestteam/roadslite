/*
 * Item.cpp
 *
 *  Created on: Apr 12, 2018
 *      Author: michael
 */

#include "Item.h"

#include "../GameObject.h"

Item::Item(NCursesApplication* app, GameManager* gm, Point position)
	: GameObject(app, gm, position),
	  typeTags(),
	  name(""),
	  description("") {

}

Item::Item(NCursesApplication* app, GameManager* gm, Point position, char* name, char* description)
	: GameObject(app, gm, position),
	  typeTags(),
	  name(name),
	  description(description) {

}

Item::~Item() {

}
