/*
 * Weapon.h
 *
 *  Created on: Apr 13, 2018
 *      Author: michael
 */

#ifndef MODEL_ITEMS_WEAPON_H_
#define MODEL_ITEMS_WEAPON_H_

#include "../Item.h"

class Weapon : public Item {
public:
	Weapon(NCursesApplication* app, GameManager* gm, Point position);
};

#endif /* MODEL_ITEMS_WEAPON_H_ */
