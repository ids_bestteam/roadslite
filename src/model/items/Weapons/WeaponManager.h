/*
 * WeaponManager.h
 *
 *  Created on: Apr 19, 2018
 *      Author: michael
 */

#ifndef MODEL_ITEMS_WEAPONS_WEAPONMANAGER_H_
#define MODEL_ITEMS_WEAPONS_WEAPONMANAGER_H_

#include <map>

#include "../Item.h"
#include "Weapon.h"

class WeaponManager {
public:
	~WeaponManager();
private:
	std::map<Item*, Weapon*> weapons;
};

#endif /* MODEL_ITEMS_WEAPONS_WEAPONMANAGER_H_ */
