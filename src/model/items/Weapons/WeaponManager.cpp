/*
 * WeaponManager.cpp
 *
 *  Created on: Apr 19, 2018
 *      Author: michael
 */

#include "WeaponManager.h"

/**
 * WeaponManager destructor
 * Performs garbage collection on all weapons
 */
WeaponManager::~WeaponManager() {
	// We know that each item is associated with exactly one weapon
	// and vice versa.
	for (std::pair<Item*, Weapon*> pair : weapons) {
		Weapon* weapon = pair.second;
		delete weapon;
	}
}
