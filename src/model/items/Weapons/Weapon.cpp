/*
 * Weapon.cpp
 *
 *  Created on: Apr 13, 2018
 *      Author: michael
 */

#include "Weapon.h"

Weapon::Weapon(NCursesApplication* app, GameManager* gm, Point position)
	: Item(app, gm, position) {
	typeTags.set(TYPES::WEAPON);
}

