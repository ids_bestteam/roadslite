/*
 * ItemManager.h
 *
 *  Created on: Apr 19, 2018
 *      Author: michael
 */

#ifndef MODEL_ITEMS_ITEMMANAGER_H_
#define MODEL_ITEMS_ITEMMANAGER_H_

#include <map>

#include "Item.h"

#include "Crafting/CraftingManager.h"
#include "Weapons/WeaponManager.h"

#include "../../util/Point.h"

class ItemManager {
public:
	~ItemManager();
	void loadItems();
	void destroyItem(Item* item);

private:
	const char* ITEMS_FILE = "items.txt";
	std::string ITEMS_REGEX = "(.) `([^`]*)` ([01]{8})";
	std::map<Point, Item*> items;

	CraftingManager craftingManager;
	WeaponManager weaponManager;
};

#endif /* MODEL_ITEMS_ITEMMANAGER_H_ */
