/*
 * ConsumableManager.cpp
 *
 *  Created on: Apr 19, 2018
 *      Author: michael
 */

#include "ConsumableManager.h"

ConsumableManager::~ConsumableManager() {
	for (std::pair<Item*, Consumable*> pair : consumables) {
		Consumable* consumable = pair.second;
		delete consumable;
	}
	consumables.clear();
}

void ConsumableManager::consumeItem(Item* item) {
	// TODO: implement this
	itemManager -> destroyItem(item);
}

