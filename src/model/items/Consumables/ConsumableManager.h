/*
 * ConsumableManager.h
 *
 *  Created on: Apr 19, 2018
 *      Author: michael
 */

#ifndef MODEL_ITEMS_CONSUMABLES_CONSUMABLEMANAGER_H_
#define MODEL_ITEMS_CONSUMABLES_CONSUMABLEMANAGER_H_

#include <map>

#include "Consumable.h"

#include "../ItemManager.h"

class ConsumableManager {
public:
	virtual ~ConsumableManager();

private:
	ItemManager* itemManager;
	std::map<Item*, Consumable*> consumables;

	void consumeItem(Item* item);
};

#endif /* MODEL_ITEMS_CONSUMABLES_CONSUMABLEMANAGER_H_ */
