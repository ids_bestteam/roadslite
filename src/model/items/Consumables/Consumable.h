/*
 * Consumable.h
 *
 *  Created on: Apr 12, 2018
 *      Author: michael
 */

#ifndef MODEL_CONSUMABLE_H_
#define MODEL_CONSUMABLE_H_

#include "../Item.h"

class Consumable: public Item {
public:
	Consumable(NCursesApplication* app, GameManager* gm, Point position);
	virtual ~Consumable() {}

	virtual void onConsume() = 0;
};

#endif /* MODEL_CONSUMABLE_H_ */
