/*
 * Consumable.cpp
 *
 *  Created on: Apr 12, 2018
 *      Author: michael
 */

#include "Consumable.h"

Consumable::Consumable(NCursesApplication* app, GameManager* gm, Point position)
	: Item(app, gm, position) {
	typeTags &= TYPES::CONSUMABLE;
}
