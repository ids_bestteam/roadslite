/*
 * Player.cpp
 *
 *  Created on: Apr 7, 2018
 *      Author: michael
 */
#include <iostream>
#include <ncurses.h>	// For key constants

#include "Player.h"
#include "../UI/NCursesApplication.h"

Player::Player(NCursesApplication* app, GameManager* gm, Point position)
	: GameObject(app, gm, position) {
}

Player* Player::instance = NULL;

Player* Player::getInstance() {
	if (instance == NULL) {
		throw "Instance should not be null in call to getInstance with no params";
	}
	return instance;
}

Player* Player::getInstance(NCursesApplication* app, GameManager* gm, Point position) {
	if (instance == NULL)
		instance = new Player(app, gm, position);
	return instance;
}

char Player::getSprite() {
	return '@';
}

void Player::handleKeyEvent(int keyCode) {
	prevPosition = position;
	switch (keyCode) {
	case 'w': case 'W': case KEY_UP:
		position.y--;
		notifyObservers((void*) &prevPosition);
		break;
	case 's': case 'S': case KEY_DOWN:
		position.y++;
		notifyObservers((void*) &prevPosition);
		break;
	case 'a': case 'A': case KEY_LEFT:
		position.x--;
		notifyObservers((void*) &prevPosition);
		break;
	case 'd': case 'D': case KEY_RIGHT:
		position.x++;
		notifyObservers((void*) &prevPosition);
		break;
	default: break;
	}
//	app -> debug(std::to_string(position.x) + ":" + std::to_string(position.y));
//	app -> debug1(std::to_string(prevPosition.x) + ":" + std::to_string(prevPosition.y));


}
