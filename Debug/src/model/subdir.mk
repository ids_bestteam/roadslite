################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/model/GameManager.cpp \
../src/model/GameObject.cpp \
../src/model/Player.cpp 

OBJS += \
./src/model/GameManager.o \
./src/model/GameObject.o \
./src/model/Player.o 

CPP_DEPS += \
./src/model/GameManager.d \
./src/model/GameObject.d \
./src/model/Player.d 


# Each subdirectory must supply rules for building sources it contributes
src/model/%.o: ../src/model/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


