################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/model/items/Consumables/Consumable.cpp \
../src/model/items/Consumables/ConsumableManager.cpp 

OBJS += \
./src/model/items/Consumables/Consumable.o \
./src/model/items/Consumables/ConsumableManager.o 

CPP_DEPS += \
./src/model/items/Consumables/Consumable.d \
./src/model/items/Consumables/ConsumableManager.d 


# Each subdirectory must supply rules for building sources it contributes
src/model/items/Consumables/%.o: ../src/model/items/Consumables/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


