################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/model/items/Crafting/CraftingItem.cpp \
../src/model/items/Crafting/CraftingManager.cpp 

OBJS += \
./src/model/items/Crafting/CraftingItem.o \
./src/model/items/Crafting/CraftingManager.o 

CPP_DEPS += \
./src/model/items/Crafting/CraftingItem.d \
./src/model/items/Crafting/CraftingManager.d 


# Each subdirectory must supply rules for building sources it contributes
src/model/items/Crafting/%.o: ../src/model/items/Crafting/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


