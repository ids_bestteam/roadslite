################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/model/items/Item.cpp \
../src/model/items/ItemManager.cpp 

OBJS += \
./src/model/items/Item.o \
./src/model/items/ItemManager.o 

CPP_DEPS += \
./src/model/items/Item.d \
./src/model/items/ItemManager.d 


# Each subdirectory must supply rules for building sources it contributes
src/model/items/%.o: ../src/model/items/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++11 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


